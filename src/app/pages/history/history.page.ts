import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  @ViewChild('lineCanvas', { static: true }) lineCanvas;


  constructor(private router: Router) { }
  dashboardPage() {
    this.router.navigate(['dashboard'])
  }
  notificationsPage() {
    this.router.navigate(['notifications'])
  }
  profilePage() {
    this.router.navigate(['profile'])
  }
  ngOnInit() { }

}
