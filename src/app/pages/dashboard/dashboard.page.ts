import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  @ViewChild("barCanvas", { static: true }) barCanvas;

  public menustatus: any;
  constructor(private router: Router) { }
  menuopen() {
    alert("function changecolor");
    this.menustatus = 'open';
  }
  notificationsPage() {
    this.router.navigate(['notifications'])
  }
  profilePage() {
    this.router.navigate(['profile'])
  }

  ngOnInit() { }

}
